var modal = document.getElementById('modal-price');
var btn = document.getElementById("price-btn");
var span = document.getElementsByClassName("close-modal")[0];

btn.onclick = function () {
  modal.style.display = "flex";
  document.body.classList.add("show-modal");
  return false;
}

span.onclick = function () {
  modal.style.display = "none";
  document.body.classList.remove("show-modal");
}

window.onclick = function (event) {
  if (event.target == modal) {
    modal.style.display = "none";
    document.body.classList.remove("show-modal");
  }
}

window.onkeydown = function (event) {
  if (event.keyCode == 27) {
    modal.style.display = "none";
    document.body.classList.remove("show-modal");
  }
};

ymaps.ready(init);

function init() {
  var myMap = new ymaps.Map("map", {
    center: [53.932144, 27.461380],
    zoom: 18
  });
  myMap.geoObjects.add(new ymaps.Placemark([53.931847, 27.460587], {
    iconCaption: 'Магазин "Медведь"'
  }, {
    preset: 'islands#redShoppingIcon'
  }))
  myMap.behaviors.disable('scrollZoom');
  if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    myMap.behaviors.disable('drag');
  }
}

var gallery = $('.shop-images__gallery a').simpleLightbox({
  showCounter: false
});
var galleryBtn = document.getElementById('open-gallery');

galleryBtn.onclick = function () {
  gallery.open();
}


$('#ecocard').click(function() {
  $('body, html').animate({
    scrollTop: $("#ecocard-promo").offset().top
  }, 800);
  return false;
});